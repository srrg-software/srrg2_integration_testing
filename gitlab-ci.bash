#!/bin/bash

# https://docs.gitlab.com/ce/ci/variables/README.html

# Check CI project directory
#---------------------------
if [[ -z "$CI_PROJECT_DIR" ]]; then
  echo "CI_PROJECT_DIR variable is empty"
  exit 1
fi

# Manage self testing
#--------------------
if [[ "$CI_PROJECT_NAME" == "srrg2_integration" ]]; then
  echo "##############################################"
  SELF_TESTING="true"

  # Delete sub ROS GitLab CI repository
  rm -rf $CI_PROJECT_DIR/$CI_PROJECT_NAME

  # clone srrg2 packages 
  git clone git@gitlab.com:srrg-software/srrg2_scripts.git
  cd srrg2_scripts
  source srrg_setup.bash configs/packages_continuous_integration.bash

  cd ~/workspaces/srrg2
  rm -rf build/ devel/ src/CMakelists.txt 

#  We create a package beginner_tutorials so that the catkin workspace is not empty
#  mkdir -p $CI_PROJECT_DIR/catkin_workspace/src
#  cd $CI_PROJECT_DIR/catkin_workspace/src
#  git clone git@gitlab.com:srrg-software/srrg_cmake_modules.git
#  catkin_create_pkg mannaggia_cristo std_msgs rospy roscpp
#  cd $CI_PROJECT_DIR/catkin_workspace
  echo "##############################################"

  # ccache
  #-------
  if [[ -e "$DISABLE_CCACHE" ]]; then
    source $CI_PROJECT_DIR/ccache.bash
  fi

  # Source the gitlab-ros script from the sub GitLab repository
  # This repository has the right branch
  source $CI_PROJECT_DIR/gitlab-ros.bash
else
  # ccache
  #-------
  if [[ -e "$DISABLE_CCACHE" ]]; then
    source srrg2_integration/ccache.bash
  fi

  source srrg2_integration/gitlab-ros.bash
fi
