# SRRG2 Continuous Integration and Testing

[![pipeline status](https://gitlab.com/srrg-software/srrg2_integration/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_integration/commits/master)

Every morning (4am UTC+1) the runner clones all the `srrg2` packages listed in [srrg2_scripts](https://gitlab.com/srrg-software/srrg2_scripts) and perform two consecutive builds on **ros-kinetic (16.04)** and **ros-melodic (18.04)**:

* `catkin_make`
* `catkin build`

## Todo

* Add further configurations (ros lunar)
* Add Unit Testing in this repo (`gtest`)

# Dockers
## [dockers](https://gitlab.com/srrg-internals/dockers)
master: [![pipeline status](https://gitlab.com/srrg-internals/dockers/badges/master/pipeline.svg)](https://gitlab.com/srrg-internals/dockers/commits/master)
### This must be always green!!! (if not burst all the srrg related docker images from the server and make it compile again <3 )

# Pieplines
## [srrg2_core](https://gitlab.com/srrg-software/srrg2_core)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_core/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_core/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_core/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_core/commits/devel)

## [srrg2_state_machine](https://gitlab.com/srrg-software/srrg2_state_machine)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_state_machine/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_state_machine/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_state_machine/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_state_machine/commits/devel)

## [srrg2_qgl_viewport](https://gitlab.com/srrg-software/srrg2_qgl_viewport)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_qgl_viewport/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_qgl_viewport/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_qgl_viewport/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_qgl_viewport/commits/devel)

## [srrg2_solver](https://gitlab.com/srrg-software/srrg2_solver)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_solver/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_solver/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_solver/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_solver/commits/devel)

## [srrg2_apriltag_calibration](https://gitlab.com/srrg-software/srrg2_apriltag_calibration)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_apriltag_calibration/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_apriltag_calibration/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_apriltag_calibration/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_apriltag_calibration/commits/devel)

## [srrg2_slam_interfaces](https://gitlab.com/srrg-software/srrg2_slam_interfaces)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_slam_interfaces/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_slam_interfaces/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_slam_interfaces/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_slam_interfaces/commits/devel)

## [srrg2_nw_calibration](https://gitlab.com/srrg-software/srrg2_nw_calibration)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_nw_calibration/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_nw_calibration/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_nw_calibration/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_nw_calibration/commits/devel)

## [srrg2_laser_slam_2d](https://gitlab.com/srrg-software/srrg2_laser_slam_2d)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_laser_slam_2d/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_laser_slam_2d/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_laser_slam_2d/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_laser_slam_2d/commits/devel)

## [srrg2_proslam](https://gitlab.com/srrg-software/srrg2_proslam)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_proslam/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_proslam/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_proslam/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_proslam/commits/devel)

## [srrg2_shaslam](https://gitlab.com/srrg-software/srrg2_shaslam)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_shaslam/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_shaslam/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_shaslam/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_shaslam/commits/devel)

## [srrg2_mpr](https://gitlab.com/srrg-software/srrg2_mpr)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_mpr/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_mpr/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_mpr/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_mpr/commits/devel)

## [srrg2_nicp](https://gitlab.com/srrg-software/srrg2_nicp)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_nicp/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_nicp/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_nicp/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_nicp/commits/devel)

## [srrg2_finslam](https://gitlab.com/srrg-software/srrg2_finslam)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_finslam/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_finslam/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_finslam/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_finslam/commits/devel)

## [srrg2_executor](https://gitlab.com/srrg-software/srrg2_executor)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_executor/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_executor/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_executor/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_executor/commits/devel)

## [srrg2_config_visualizer](https://gitlab.com/srrg-software/srrg2_config_visualizer)
master: [![pipeline status](https://gitlab.com/srrg-software/srrg2_config_visualizer/badges/master/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_config_visualizer/commits/master)
devel: [![pipeline status](https://gitlab.com/srrg-software/srrg2_config_visualizer/badges/devel/pipeline.svg)](https://gitlab.com/srrg-software/srrg2_config_visualizer/commits/devel)

# Other stuff
## [srrg2_slam_architecture](https://gitlab.com/mircolosi/srrg2_slam_architecture)
master: [![pipeline status](https://gitlab.com/mircolosi/srrg2_slam_architecture/badges/master/pipeline.svg)](https://gitlab.com/mircolosi/srrg2_slam_architecture/commits/master)
devel: [![pipeline status](https://gitlab.com/mircolosi/srrg2_slam_architecture/badges/devel/pipeline.svg)](https://gitlab.com/mircolosi/srrg2_slam_architecture/commits/devel)
